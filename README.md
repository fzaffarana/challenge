# challenge

## Deploy 

```
git clone [project-url]
cd [project-name]
npm install
npm start
```

## Optative:

### strategy for logging
I recommend using pino (https://github.com/pinojs/pino) to loggin. 
It is faster than the others in high recurrence (https://github.com/pinojs/pino/blob/master/docs/benchmarks.md). Also, it accepts the next loggin methods: trace, debug, info, warn, and fatal.

If you need to rotate logs to a file in disk, I would recommend using winston (https://github.com/winstonjs) + winston-daily-rotate-file (https://github.com/winstonjs/winston-daily-rotate-file).

### strategy for collecting usage statistics
I recommend using Datadog (https://www.datadoghq.com/) to statistics. I had good results using it with restify (http://restify.com/). 

More info: https://docs.datadoghq.com/integrations/node/

### strategy of deployment of the services
In this moment I am using Codeship for CD (https://codeship.com/). 

This provides a series of steps, where you can use bash code to do what you want: install node modules, testing, handle server instances.