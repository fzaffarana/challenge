'use strict'

// Modules
const app = require('./app')

// Config values
const { host, port } = require(`./config/config.${process.env.ENVIRONMENT || 'prod'}.js`)

const start = async () => {
  try {
    await app.listen(port, host)
    console.log(`Listening on http://${host}:${port}`)
  } catch (err) {
    console.error(err)
    process.exit(1)
  }
}

start()
