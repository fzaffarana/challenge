'use strict'

const express = require('express')
const api = express.Router()

const {
  getPeopleById,
  getPeople
} = require('../../controllers/people')

api.get('/', getPeople)
api.get('/:id', getPeopleById)

module.exports = api
