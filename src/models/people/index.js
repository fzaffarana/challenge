'use strict'

// Modules
const rp = require('request-promise')
const url = require('url')

// Config Values
const { URI } = require(`../../config/config.${process.env.ENVIRONMENT || 'prod'}.js`)

class People {
  constructor () {
    this.cache = null
  }

  /**
   * Find people from swapi api
   */
  async find () {
    // Check if data exists in cache
    if (this.cache) return this.cache

    let data = []
    let next = true
    let uri = URI

    while (next) {
      // Fetch data from api until next page doesn't exist
      let res = await rp.get({
        uri,
        json: true
      })

      next = !!res.next
      uri = res.next

      if (res.results && res.results.length) {
        data = [...data, ...res.results]
      }
    }

    // If data contains some value, save it in cache
    if (data.length) {
      this.cache = data.map(({ name, url: dataUrl }) => {
        return {
          name,
          id: url.parse(dataUrl).pathname.split('/')[3]
        }
      })
    }

    return this.cache
  }

  /**
   * Find people from swapi api (by id)
   */
  async findById (id) {
    let res = await rp.get({
      uri: `${URI}${id}`,
      json: true
    })

    // I could use a hateoas module to get the extra info
    const fields = ['homeworld', 'films', 'species', 'vehicles', 'starships']

    const augmentData = await Promise.all(fields.map(async field => {
      let result = res[field].length ? await this.augmentData(field, Array.isArray(res[field]) ? res[field] : [res[field]]) : []
      return result
    }))

    const [homeworld, films, species, vehicles, starships] = augmentData

    return {
      ...res,
      homeworld,
      films,
      species,
      vehicles,
      starships
    }
  }

  async augmentData (field, uris) {
    const fieldMapping = {
      films: 'title',
      default: 'name'
    }

    const promises = uris.map(async uri => {
      const response = await rp.get({
        uri,
        json: true
      })

      return {
        name: response[fieldMapping[field] || fieldMapping['default']],
        id: url.parse(uri).pathname.split('/')[3]
      }
    })

    const res = await Promise.all(promises)
    return res
  }
}

module.exports = new People()
