'use strict'

// Models
const People = require('../../models/people')

const getPeople = async (req, res) => {
  try {
    const data = await People.find()
    return res.send(data)
  } catch (error) {
    console.error('There was an error:', error)
    return res.status(500).send({
      message: 'Sorry, there was an error processing your request. Please try again later'
    })
  }
}

const getPeopleById = async (req, res) => {
  try {
    const { id } = req.params
    const data = await People.findById(id)
    return res.send(data)
  } catch (error) {
    if (error.statusCode) {
      return res.status(error.statusCode).send({
        message: error.error.detail
      })
    }

    console.error('There was an error:', error)
    return res.status(500).send({
      message: 'Sorry, there was an error processing your request. Please try again later'
    })
  }
}

module.exports = {
  getPeople,
  getPeopleById
}
