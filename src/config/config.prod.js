'use strict'

module.exports = {
  host: 'localhost',
  port: 3000,
  URI: 'https://swapi.co/api/people/'
}
